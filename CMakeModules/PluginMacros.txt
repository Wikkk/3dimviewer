#===============================================================================
# $Id: PluginMacros.txt 1292 2011-05-15 17:13:39Z spanel $
#
# 3DimViewer
# Lightweight 3D DICOM viewer.
#
# Copyright 2008-2012 3Dim Laboratory s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#===============================================================================

#-------------------------------------------------------------------------------
# Used macros

# Library name macro
macro( TRIDIM_PLUGIN _PLUGIN_NAME )
    set( TRIDIM_PLUGIN_NAME ${_PLUGIN_NAME} )
    string( REPLACE Plugin "" _SHORT_PLUGIN_NAME ${_PLUGIN_NAME} )
    set( TRIDIM_PLUGIN_PROJECT_NAME plugin${_SHORT_PLUGIN_NAME} )
    set( TRIDIM_PLUGIN_HEADERS "" )
    set( TRIDIM_PLUGIN_SOURCES "" )
    set( TRIDIM_PLUGIN_UI_FILES "" )
    set( TRIDIM_PLUGIN_RC_FILES "" )
    set( TRIDIM_PLUGIN_TRANSLATION_FILES "" )
    set( TRIDIM_PLUGIN_MOC_SOURCES "" )
    set( TRIDIM_PLUGIN_UI_SOURCES "" )
    set( TRIDIM_PLUGIN_RC_SOURCES "" )
endmacro( TRIDIM_PLUGIN )


# Add plugin source file
macro( ADD_PLUGIN_SOURCE_FILE )
    set( TRIDIM_PLUGIN_SOURCES ${TRIDIM_PLUGIN_SOURCES} ${ARGV} )
endmacro( ADD_PLUGIN_SOURCE_FILE )

# Add header file to the plugin
macro( ADD_PLUGIN_HEADER_FILE )
    set( TRIDIM_PLUGIN_HEADERS ${TRIDIM_PLUGIN_HEADERS} ${ARGV} )
endmacro( ADD_PLUGIN_HEADER_FILE )

# Add UI file to the plugin
macro( ADD_PLUGIN_UI_FILE )
    set( TRIDIM_PLUGIN_UI_FILES ${TRIDIM_PLUGIN_UI_FILES} ${ARGV} )
endmacro( ADD_PLUGIN_UI_FILE )

# Add RC file to the plugin
macro( ADD_PLUGIN_RC_FILE )
    set( TRIDIM_PLUGIN_RC_FILES ${TRIDIM_PLUGIN_RC_FILES} ${ARGV} )
endmacro( ADD_PLUGIN_RC_FILE )

# Add translation file to the plugin
macro( ADD_PLUGIN_TRANSLATION_FILE )
    set( TRIDIM_PLUGIN_TRANSLATION_FILES ${TRIDIM_PLUGIN_TRANSLATION_FILES} ${ARGV} )
endmacro( ADD_PLUGIN_TRANSLATION_FILE )


# Add sources directory - adds all source files from the directory
macro( ADD_PLUGIN_SOURCE_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_PLUGIN_SOURCES ${_DIR}/*.c ${_DIR}/*.cpp ${_DIR}/*.cc )
    list( APPEND TRIDIM_PLUGIN_SOURCES ${_TRIDIM_PLUGIN_SOURCES} )
endmacro( ADD_PLUGIN_SOURCE_DIRECTORY )

# Add include directory - adds all headers from the directory
macro( ADD_PLUGIN_HEADER_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_PLUGIN_HEADERS ${_DIR}/*.h ${_DIR}/*.hxx ${_DIR}/*.hpp )
    list( APPEND TRIDIM_PLUGIN_HEADERS ${_TRIDIM_PLUGIN_HEADERS} )
endmacro( ADD_PLUGIN_HEADER_DIRECTORY )

# Adds all UI files from the directory
macro( ADD_PLUGIN_UI_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_PLUGIN_UI_FILES ${_DIR}/*.ui )
    list( APPEND TRIDIM_PLUGIN_UI_FILES ${_TRIDIM_PLUGIN_UI_FILES} )
endmacro( ADD_PLUGIN_UI_DIRECTORY )

# Adds all RC files from the directory
macro( ADD_PLUGIN_RC_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_PLUGIN_RC_FILES ${_DIR}/*.rc )
    list( APPEND TRIDIM_PLUGIN_RC_FILES ${_TRIDIM_PLUGIN_RC_FILES} )
endmacro( ADD_PLUGIN_RC_DIRECTORY )

# Adds all translation files from the directory
macro( ADD_PLUGIN_TRANSLATION_DIRECTORY _DIR )
    file( GLOB_RECURSE _TRIDIM_PLUGIN_TRANSLATION_FILES ${_DIR}/*.ts )
    list( APPEND TRIDIM_PLUGIN_TRANSLATION_FILES ${_TRIDIM_PLUGIN_TRANSLATION_FILES} )
endmacro( ADD_PLUGIN_TRANSLATION_DIRECTORY )


# Add dependency
macro( ADD_PLUGIN_DEPENDENCY _LIB )
    target_link_libraries( ${TRIDIM_PLUGIN_NAME} ${_LIB} )
endmacro( ADD_PLUGIN_DEPENDENCY )


# Build macro
macro( TRIDIM_PLUGIN_BUILD )
    set( CMAKE_CFG_INTDIR "/" )
    add_library( ${TRIDIM_PLUGIN_NAME} SHARED ${TRIDIM_PLUGIN_SOURCES} ${TRIDIM_PLUGIN_HEADERS} )
    set_target_properties( ${TRIDIM_PLUGIN_NAME} PROPERTIES
                           RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/pluginsd/"
                           RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/pluginsd/"
                           LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           PROJECT_LABEL ${TRIDIM_PLUGIN_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_PLUGIN_BUILD )

# Qt build macro
macro( TRIDIM_QT4_PLUGIN_BUILD )
    # process headers by MOC and generate list of resulting source files
    QT4_WRAP_CPP( TRIDIM_PLUGIN_MOC_SOURCES ${TRIDIM_PLUGIN_HEADERS} )
    
    # UI files are processed to headers and sources
    QT4_WRAP_UI( TRIDIM_PLUGIN_UI_SOURCES ${TRIDIM_PLUGIN_UI_FILES} )
    
    # same applies to resources
    QT4_ADD_RESOURCES( TRIDIM_PLUGIN_RC_SOURCES ${TRIDIM_PLUGIN_RC_FILES} )
    
    #-------------------------------------------------------------------------------
    # setup translations
    # http://www.cmake.org/Wiki/CMake:How_To_Build_Qt4_Software
    
    # files to translate
    set( FILES_TO_TRANSLATE ${TRIDIM_PLUGIN_SOURCES} ${TRIDIM_PLUGIN_UI_FILES} )

    # !!! when UPDATE_TRANSLATIONS is on then ts files are generated
    # !!! from source files and rebuild erases them completely
    set( QM_FILES "" )
    if( UPDATE_TRANSLATIONS )
      message(warning " creating translations ${TRIDIM_PLUGIN_TRANSLATION_FILES}")
      qt4_create_translation( QM_FILES ${FILES_TO_TRANSLATE} ${TRIDIM_PLUGIN_TRANSLATION_FILES} )
    else( UPDATE_TRANSLATIONS )
      qt4_add_translation( QM_FILES ${TRIDIM_PLUGIN_TRANSLATION_FILES} )
    endif( UPDATE_TRANSLATIONS )

    # add defition saying that this is a plugin
    ADD_DEFINITIONS( -DQT_PLUGIN )
    ADD_DEFINITIONS( -DQT_SHARED )

    # add include directory
    INCLUDE_DIRECTORIES( ${CMAKE_CURRENT_BINARY_DIR} )

    set( CMAKE_CFG_INTDIR "/" )
    add_library( ${TRIDIM_PLUGIN_NAME} SHARED
                 ${TRIDIM_PLUGIN_SOURCES} 
                 ${TRIDIM_PLUGIN_HEADERS} 
                 ${TRIDIM_PLUGIN_MOC_SOURCES} 
                 ${TRIDIM_PLUGIN_UI_SOURCES} 
                 ${TRIDIM_PLUGIN_RC_SOURCES}
                 ${QM_FILES} 
                 )
    set_target_properties( ${TRIDIM_PLUGIN_NAME} PROPERTIES
                           RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/pluginsd/"
                           RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/pluginsd/"
                           LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           PROJECT_LABEL ${TRIDIM_PLUGIN_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_QT4_PLUGIN_BUILD )

# Qt build macro
macro( TRIDIM_QT5_PLUGIN_BUILD )
    # process headers by MOC and generate list of resulting source files
    QT5_WRAP_CPP( TRIDIM_PLUGIN_MOC_SOURCES ${TRIDIM_PLUGIN_HEADERS} )
    
    # UI files are processed to headers and sources
    QT5_WRAP_UI( TRIDIM_PLUGIN_UI_SOURCES ${TRIDIM_PLUGIN_UI_FILES} )
    
    # same applies to resources
    QT5_ADD_RESOURCES( TRIDIM_PLUGIN_RC_SOURCES ${TRIDIM_PLUGIN_RC_FILES} )
    
    #-------------------------------------------------------------------------------
    # setup translations
    # http://www.cmake.org/Wiki/CMake:How_To_Build_Qt4_Software
    # http://doc-snapshot.qt-project.org/5.0/qtdoc/cmake-manual.html
    
    # files to translate
    set( FILES_TO_TRANSLATE ${TRIDIM_PLUGIN_SOURCES} ${TRIDIM_PLUGIN_UI_FILES} )

    # !!! when UPDATE_TRANSLATIONS is on then ts files are generated
    # !!! from source files and rebuild erases them completely
    set( QM_FILES "" )
    if( UPDATE_TRANSLATIONS )
      message(warning " creating translations ${TRIDIM_PLUGIN_TRANSLATION_FILES}")
      qt5_create_translation( QM_FILES ${FILES_TO_TRANSLATE} ${TRIDIM_PLUGIN_TRANSLATION_FILES} )
    else( UPDATE_TRANSLATIONS )
      qt5_add_translation( QM_FILES ${TRIDIM_PLUGIN_TRANSLATION_FILES} )
    endif( UPDATE_TRANSLATIONS )

    # add defition saying that this is a plugin
    ADD_DEFINITIONS( -DQT_PLUGIN )
    ADD_DEFINITIONS( -DQT_SHARED )

    # add include directory
    INCLUDE_DIRECTORIES( ${CMAKE_CURRENT_BINARY_DIR} )

    set( CMAKE_CFG_INTDIR "/" )
    add_library( ${TRIDIM_PLUGIN_NAME} SHARED
                 ${TRIDIM_PLUGIN_SOURCES} 
                 ${TRIDIM_PLUGIN_HEADERS} 
                 ${TRIDIM_PLUGIN_MOC_SOURCES} 
                 ${TRIDIM_PLUGIN_UI_SOURCES} 
                 ${TRIDIM_PLUGIN_RC_SOURCES}
                 ${QM_FILES} 
                 )
    set_target_properties( ${TRIDIM_PLUGIN_NAME} PROPERTIES
                           RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/pluginsd/"
                           RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/pluginsd/"
                           LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO "${CMAKE_CURRENT_SOURCE_DIR}/plugins/"
                           PROJECT_LABEL ${TRIDIM_PLUGIN_PROJECT_NAME}
                           DEBUG_POSTFIX d
                           LINK_FLAGS "${TRIDIM_LINK_FLAGS}"
                           )
endmacro( TRIDIM_QT5_PLUGIN_BUILD )

macro ( TRIDIM_QT_PLUGIN_BUILD )
  if (BUILD_WITH_QT5)
    TRIDIM_QT5_PLUGIN_BUILD()
  else (BUILD_WITH_QT5)
    TRIDIM_QT4_PLUGIN_BUILD()
  endif (BUILD_WITH_QT5)
endmacro ( TRIDIM_QT_PLUGIN_BUILD )


# Install macro
macro( TRIDIM_PLUGIN_INSTALL )
    if( MSVC )
        install( TARGETS ${TRIDIM_PLUGIN_NAME} RUNTIME DESTINATION plugins )
    endif( MSVC )
endmacro( TRIDIM_PLUGIN_INSTALL )

# Qt install macro
macro( TRIDIM_QT_PLUGIN_INSTALL )
    if( MSVC )
        install( TARGETS ${TRIDIM_PLUGIN_NAME} RUNTIME DESTINATION plugins )
    endif( MSVC )
endmacro( TRIDIM_QT_PLUGIN_INSTALL )
