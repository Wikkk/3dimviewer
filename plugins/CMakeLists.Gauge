#===============================================================================
# $Id$
#
# 3DimViewer
# Lightweight 3D DICOM viewer.
#
# Copyright 2008-2012 3Dim Laboratory s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#===============================================================================

# Create plugin
TRIDIM_PLUGIN( GaugePlugin )

# Options
set( TRIDIM_THIS_PLUGIN_PATH ${TRIDIM_OSS_PLUGINS_PATH}/Gauge )
set( TRIDIM_THIS_PLUGIN_INCLUDE "" )
set( TRIDIM_THIS_PLUGIN_SRC "" )

# Include directory
include_directories( ${TRIDIM_THIS_PLUGIN_PATH} )

#-------------------------------------------------------------------------------
# Find required 3rd party libraries

ADD_LIB_OPENMESH()

#-------------------------------------------------------------------------------
# Add required 3Dim libraries

ADD_3DIM_LIB_TARGET( ${TRIDIM_CORE_LIB} )
ADD_3DIM_LIB_TARGET( ${TRIDIM_COREMEDI_LIB} )
ADD_3DIM_LIB_TARGET( ${TRIDIM_GRAPH_LIB} )
ADD_3DIM_LIB_TARGET( ${TRIDIM_GRAPHMEDI_LIB} )
ADD_3DIM_LIB_TARGET( ${TRIDIM_GUIQT_LIB} )
ADD_3DIM_LIB_TARGET( ${TRIDIM_GUIQTMEDI_LIB} )

#-------------------------------------------------------------------------------
# Add Headers and Sources

ADD_PLUGIN_HEADER_DIRECTORY( ${TRIDIM_THIS_PLUGIN_PATH}${TRIDIM_THIS_PLUGIN_INCLUDE} )  
ADD_PLUGIN_SOURCE_DIRECTORY( ${TRIDIM_THIS_PLUGIN_PATH}${TRIDIM_THIS_PLUGIN_SRC} )

#-------------------------------------------------------------------------------
# Some qt-related stuff

ADD_PLUGIN_UI_DIRECTORY( ${TRIDIM_THIS_PLUGIN_PATH} )
ADD_PLUGIN_RC_FILE( ${TRIDIM_THIS_PLUGIN_PATH}/gauge.qrc )
ADD_PLUGIN_TRANSLATION_FILE( ${TRIDIM_THIS_PLUGIN_PATH}/translations/gaugeplugin-cs_cz.ts )

# !!! when UPDATE_TRANSLATIONS is on then ts files are generated
# from source files and rebuild erases them completely !!!
#set( UPDATE_TRANSLATIONS 1 )

#-------------------------------------------------------------------------------
# Create source groups

SOURCE_GROUP( "Header Files" REGULAR_EXPRESSION "^dummyrule$" )
SOURCE_GROUP( "Source Files" REGULAR_EXPRESSION "^dummyrule$" )

ADD_SOURCE_GROUPS( ${TRIDIM_PLUGIN_HEADERS}
                   ${TRIDIM_PLUGIN_SOURCES}
                   )

#-------------------------------------------------------------------------------
# Finalize plugin

# Build library 
TRIDIM_QT_PLUGIN_BUILD()

# Add dependencies
add_dependencies( GaugePlugin
                  ${TRIDIM_CORE_LIB}
                  ${TRIDIM_COREMEDI_LIB}
                  ${TRIDIM_GRAPH_LIB}
                  ${TRIDIM_GRAPHMEDI_LIB}
                  ${TRIDIM_GUIQT_LIB}
                  ${TRIDIM_GUIQTMEDI_LIB}
#                  ${TRIDIM_PLUGIN_LIB}
                  )

# Add libraries
target_link_libraries( GaugePlugin
                       ${QT_LIBRARIES}
                       ${QT_QTMAIN_LIBRARY}
                       ${TRIDIM_CORE_LIB}
                       ${TRIDIM_COREMEDI_LIB}
                       ${TRIDIM_GRAPH_LIB}
                       ${TRIDIM_GRAPHMEDI_LIB}
                       ${TRIDIM_GUIQT_LIB}
                       ${TRIDIM_GUIQTMEDI_LIB}
                       ${OSG_LINKED_LIBS}
                       ${VPL_LINKED_LIBS}
                       ${DCMTK_LINKED_LIBS}
                       ${GLEW_LINKED_LIBS}
                       ${OPENMESH_LINKED_LIBS}
                       ${QT_LINKED_LIBS}
                       ${QT_QTMAIN_LIBRARY}
                       )

if( WIN32 )
  target_link_libraries( GaugePlugin opengl32.lib )
endif( )


#-------------------------------------------------------------------------------
# Installation

# Installation
TRIDIM_QT_PLUGIN_INSTALL()

