#===============================================================================
# $Id$
#
# 3DimViewer
# Lightweight 3D DICOM viewer.
#
# Copyright 2008-2012 3Dim Laboratory s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#===============================================================================

# Create library
TRIDIM_LIBRARY( 3DimGraphMedi )

# Options
set( TRIDIM_GRAPHMEDI_LIB_INCLUDE include/3dim/graphmedi )
set( TRIDIM_GRAPHMEDI_LIB_SRC src/graphmedi )

include_directories( ${CMAKE_SOURCE_DIR}/${TRIDIM_GRAPHMEDI_LIB_INCLUDE} )

if( TRIDIM_LIBRARY_EXT )
  set( TRIDIM_GRAPHMEDI_LIB_INCLUDE_EXT ${TRIDIM_GRAPHMEDI_LIB_INCLUDE}${TRIDIM_LIBRARY_EXT} )
  set( TRIDIM_GRAPHMEDI_LIB_SRC_EXT ${TRIDIM_GRAPHMEDI_LIB_SRC}${TRIDIM_LIBRARY_EXT} )
  include_directories( ${TRIDIM_CORE_LIB_INCLUDE_EXT} )
endif()

#-------------------------------------------------------------------------------
# Add Headers and Sources

ADD_LIBRARY_HEADER_DIRECTORY( ${CMAKE_SOURCE_DIR}/${TRIDIM_GRAPHMEDI_LIB_INCLUDE} )
ADD_LIBRARY_SOURCE_DIRECTORY( ${CMAKE_SOURCE_DIR}/${TRIDIM_GRAPHMEDI_LIB_SRC} )

if( TRIDIM_LIBRARY_EXT )
  ADD_LIBRARY_HEADER_DIRECTORY( ${CMAKE_SOURCE_DIR}/${TRIDIM_GRAPHMEDI_LIB_INCLUDE_EXT} )
  ADD_LIBRARY_SOURCE_DIRECTORY( ${CMAKE_SOURCE_DIR}/${TRIDIM_GRAPHMEDI_LIB_SRC_EXT} )
endif()

#-------------------------------------------------------------------------------
# Create source groups

ADD_SOURCE_GROUPS( ${TRIDIM_GRAPHMEDI_LIB_INCLUDE}
                   ${TRIDIM_GRAPHMEDI_LIB_SRC}
                   data drawing osg render shaders shaders2 widgets
                   )

if( TRIDIM_LIBRARY_EXT )
  ADD_SOURCE_GROUPS( ${TRIDIM_GRAPHMEDI_LIB_INCLUDE_EXT}
                     ${TRIDIM_GRAPHMEDI_LIB_SRC_EXT}
                     drawing osg render shaders2 widgets guide
                     )
endif()

#-------------------------------------------------------------------------------
# Finalize library

# Build library 
TRIDIM_LIBRARY_BUILD()

# Installation
TRIDIM_LIBRARY_INSTALL()

